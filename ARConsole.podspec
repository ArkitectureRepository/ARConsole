Pod::Spec.new do |s|
  s.name             = 'ARConsole'
  s.version          = '1.2'
  s.summary          = 'ARConsole lets you know in real time your application logs.'
 
  s.description      = <<-DESC
ARConsole lets you know in real time your application logs. It's easy.
                       DESC
 
  s.homepage         = 'https://gitlab.com/ArkitectureRepository/ARConsole'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Alvaro Royo' => 'alvaroroyo3@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/ArkitectureRepository/ARConsole.git', :tag => s.version.to_s }
 
  s.ios.deployment_target = '10.0'

  s.source_files = 'ARconsole/Console/*.swift', 'ARconsole/Console/Views/*.swift', 'ARconsole/Console/Models/*.swift'

  s.framework = 'UIKit'
  s.framework = 'AVFoundation'
  
  #s.dependency = 'AKModel'
 
end