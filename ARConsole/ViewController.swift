//
//  ViewController.swift
//  ARConsole
//
//  Created by Alvaro on 22/8/17.
//  Copyright © 2017 Alvaro Royo. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private var warningsQueue = DispatchQueue(label: "Warnings Generator Thread", attributes: .concurrent)
    private var errorsQueue = DispatchQueue(label: "Errors Generator Thread", attributes: .concurrent)
    private var debugsQueue = DispatchQueue(label: "Debugs Generator Thread", attributes: .concurrent)
    private var othersQueue = DispatchQueue(label: "Others Generator Thread", attributes: .concurrent)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        warningGenerator()
        errorGenerator()
        debugGenerator()
        otherGenerator()
        
    }
    
    @IBAction func showConsoleAction() {
        ARPrint.sharedInstance().showConsole()
    }
    
    @IBAction func shareAction() {
        ARPrint.sharedInstance().shareLogFile { (canShare) in
            if !canShare {
                let accept = UIAlertAction(title: "Accept", style: .default, handler: nil)
                let vc = UIAlertController(title: "Share", message: "The share proccess can't be completed.\nMaybe the log file doesn't exist.\nTry it again in some minutes.", preferredStyle: .actionSheet)
                vc.addAction(accept)
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    private func warningGenerator() {
        warningsQueue.async {
            
            sleep(self.generateRandom())
            
            printWarning("This is a warning")
            
            self.warningGenerator()
            
        }
    }
    
    private func errorGenerator() {
        errorsQueue.async {
            
            sleep(self.generateRandom())
            
            printError("This is a error")
            
            self.errorGenerator()
            
        }
    }
    
    private func debugGenerator() {
        debugsQueue.async {
            
            sleep(self.generateRandom())
            
            printDebug("This is a debug message")
            
            self.debugGenerator()
            
        }
    }
    
    private func otherGenerator() {
        othersQueue.async {
            
            sleep(self.generateRandom())
            
            println("This is other message that contains two lines and a\nBreak line.", tag: "Other", colour: UIColor.purple)
            
            self.otherGenerator()
            
        }
    }

    private func generateRandom() -> UInt32 {
        let randomNumber = arc4random_uniform(30 - 5) + 5
        return randomNumber
    }

}

