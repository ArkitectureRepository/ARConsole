//
//  ARPrintString.swift
//  CapitalManagement
//
//  Created by Alvaro on 21/8/17.
//  Copyright © 2017 Alvaro Royo. All rights reserved.
//

import UIKit

public class ARPrintString {
    
    var date = Date()
    var tag = ""
    var colour = UIColor.white
    var message = ""
    var function = ""
    
    init(_ message:String, tag:String = "---", functionName:String, colour:UIColor = ARPrint.defaultColor) {
        self.tag = tag
        self.colour = colour
        self.message = message
        self.function = functionName
    }
    
    class func initialized(with dictionary:[String:Any]) -> ARPrintString? {
        
        guard
            let dat = dictionary["date"] as? Double,
            let tag = dictionary["tag"] as? String,
            let message = dictionary["message"] as? String,
            let color = dictionary["colour"] as? String,
            let function = dictionary["function"] as? String
            else {
                return nil
        }
        
        let date = Date.init(timeIntervalSince1970: dat)
        let colour = UIColor.init(color)
        
        let obj = ARPrintString(message, tag: tag, functionName: function, colour: colour)
        obj.date = date
        
        return obj
        
    }
    
    func getDateString() -> String {
        
        let df = DateFormatter()
        df.dateFormat = "dd/MM/yyyy HH:mm:ss"
        
        return df.string(from: self.date)
        
    }
    
    func toDictionary() -> [String:Any] {
        
        var dic:[String:Any] = [:]
        
        dic["date"] = self.date.timeIntervalSince1970
        dic["tag"] = self.tag
        dic["message"] = self.message
        dic["colour"] = self.colour.toHexString
        dic["function"] = self.function
        
        return dic
        
    }
    
}
