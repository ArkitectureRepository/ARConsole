//
//  Extension.swift
//  ARConsole
//
//  Created by Alvaro Royo on 22/8/17.
//  Copyright © 2017 Alvaro Royo. All rights reserved.
//

import UIKit

extension UIColor {
    var toHexString: String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return String(
            format: "#%02X%02X%02X",
            Int(r * 0xff),
            Int(g * 0xff),
            Int(b * 0xff)
        )
    }
    convenience init(_ hexColor: String) {
        
        let hex = hexColor.replacingOccurrences(of: "#", with: "")
        
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}

//Remove when AKModel was upload

class AKDiskManager {
    
    class func writeFile(data:Data!, path:String!, fileName:String!){
        
        AKDiskManager.removeFile(path + fileName)
        
        do{
            try FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
            try data.write(to: URL(fileURLWithPath: path + fileName))
        } catch {
            
            print("Error al escribir el archivo: %@",path + fileName);
            
        }
        
    }
    
    class func readFile(_ path:String!) -> Data? {
        
        var data:Data? = nil
        
        if FileManager.default.fileExists(atPath: path) {
            
            do{
                
                data = try Data.init(contentsOf: URL(fileURLWithPath: path))
                
            } catch {
                print("Error al leer el fichero: %@", path)
            }
            
        }
        
        return data
        
    }
    
    class func removeFile(_ path:String!){
        
        let fileManager = FileManager.default
        
        if fileManager.fileExists(atPath: path) {
            
            do{
                try fileManager.removeItem(atPath: path)
            } catch {
                
                print("Error al borrar el fichero: %@",path);
                
            }
            
        }
        
    }
    
}
