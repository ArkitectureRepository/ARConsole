//
//  Functions.swift
//  CapitalManagement
//
//  Created by Alvaro on 21/8/17.
//  Copyright © 2017 Alvaro Royo. All rights reserved.
//

import UIKit

public func println(_ message: String, tag:String = "", colour:UIColor = ARPrint.defaultColor, functionName:String = #function) {
    ARPrint.sharedInstance(true).addString(message, tag, functionName: functionName, colour: colour)
    
    print(message)
}

public func printError(_ message: String, functionName:String = #function) {
    println(message, tag: "Error", colour: UIColor.red, functionName: functionName)
}

public func printDebug(_ message: String, functionName:String = #function) {
    println(message, tag: "Debug", colour: UIColor.cyan, functionName: functionName)
}

public func printWarning(_ message: String, functionName:String = #function) {
    println(message, tag: "Warning", colour: UIColor.orange, functionName: functionName)
}
