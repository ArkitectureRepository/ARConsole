//
//  ARConsoleTableViewCell.swift
//  CapitalManagement
//
//  Created by Alvaro on 21/8/17.
//  Copyright © 2017 Alvaro Royo. All rights reserved.
//

import UIKit

class ARConsoleTableViewCell: UITableViewCell {

    let tagLbl = UILabel()
    let dateLbl = UILabel()
    let messageLbl = UILabel()
    
    let textFont = UIFont(name: "Courier", size: 17)!
    
    var lbls: [UILabel] = []
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        
        lbls = [self.tagLbl,self.dateLbl,self.messageLbl]
        
        let contentHeader = UIView()
        contentHeader.layer.cornerRadius = 5
        contentHeader.layer.borderWidth = 1
        contentHeader.layer.borderColor = UIColor.lightGray.cgColor
        contentHeader.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(contentHeader)
        
        self.tagLbl.font = textFont
        self.tagLbl.translatesAutoresizingMaskIntoConstraints = false
        contentHeader.addSubview(self.tagLbl)
        
        self.dateLbl.frame = CGRect(x: self.tagLbl.frame.maxX, y: 0, width: self.tagLbl.bounds.width, height: contentHeader.bounds.height)
        self.dateLbl.font = UIFont(name: "Courier", size: 11)
        self.dateLbl.textAlignment = .right
        self.dateLbl.translatesAutoresizingMaskIntoConstraints = false
        contentHeader.addSubview(self.dateLbl)
        
        let separator = UIView()
        separator.backgroundColor = UIColor.lightGray
        separator.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(separator)
        
        self.messageLbl.font = textFont
        self.messageLbl.numberOfLines = 0
        self.messageLbl.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(self.messageLbl)
        
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        //CONSTRAINTS
        
        let margins = self.contentView.layoutMarginsGuide
        
        contentHeader.heightAnchor.constraint(equalToConstant: 30).isActive = true
        contentHeader.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: -8).isActive = true
        contentHeader.topAnchor.constraint(equalTo: margins.topAnchor, constant: 0).isActive = true
        contentHeader.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: 8).isActive = true
        
        self.messageLbl.topAnchor.constraint(equalTo: contentHeader.bottomAnchor, constant: 18).isActive = true
        self.messageLbl.heightAnchor.constraint(greaterThanOrEqualToConstant: 21).isActive = true
        self.messageLbl.leadingAnchor.constraint(equalTo: contentHeader.leadingAnchor).isActive = true
        self.messageLbl.trailingAnchor.constraint(equalTo: contentHeader.trailingAnchor).isActive = true
        self.messageLbl.bottomAnchor.constraint(equalTo: margins.bottomAnchor, constant: -10).isActive = true
        
        separator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        separator.leadingAnchor.constraint(equalTo: margins.leadingAnchor, constant: -8).isActive = true
        separator.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: 8).isActive = true
        separator.topAnchor.constraint(equalTo: margins.bottomAnchor, constant: 9).isActive = true
        
        let contentMargins = contentHeader.layoutMarginsGuide
        
        self.tagLbl.heightAnchor.constraint(equalTo: contentHeader.heightAnchor, constant: 0).isActive = true
        self.tagLbl.widthAnchor.constraint(equalTo: contentHeader.widthAnchor, multiplier: 0.45, constant: 0).isActive = true
        self.tagLbl.leadingAnchor.constraint(equalTo: contentMargins.leadingAnchor, constant: 0).isActive = true
        self.tagLbl.topAnchor.constraint(equalTo: contentMargins.topAnchor, constant: -8).isActive = true
        
        self.dateLbl.heightAnchor.constraint(equalTo: self.tagLbl.heightAnchor).isActive = true
        self.dateLbl.widthAnchor.constraint(equalTo: self.tagLbl.widthAnchor).isActive = true
        self.dateLbl.trailingAnchor.constraint(equalTo: contentMargins.trailingAnchor, constant: 0).isActive = true
        self.dateLbl.topAnchor.constraint(equalTo: self.tagLbl.topAnchor).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setView(with string:ARPrintString){
        
        tagLbl.text = string.tag
        dateLbl.text = string.getDateString()
        
        let attrStringFunction = NSAttributedString(string: string.function + "\n\n", attributes: [NSAttributedStringKey.font : textFont.withSize(10)])
        let attrStringMessage = NSAttributedString(string: string.message, attributes: [NSAttributedStringKey.font : textFont])
        
        let attrString = NSMutableAttributedString()
        attrString.append(attrStringFunction)
        attrString.append(attrStringMessage)
        
        messageLbl.attributedText = attrString
        
        lbls.forEach{ $0.textColor = string.colour }
        
    }
    
    override func prepareForReuse() {
        lbls.forEach{ lbl in
            lbl.text = ""
            lbl.textColor = ARPrint.defaultColor
        }
    }
    
}
