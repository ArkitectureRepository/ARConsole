//
//  ARConsoleViewController.swift
//  CapitalManagement
//
//  Created by Alvaro on 21/8/17.
//  Copyright © 2017 Alvaro Royo. All rights reserved.
//

import UIKit

class ARConsoleViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    var consolePrints:[ARPrintString]!
    
    var tableView = UITableView()
    var startStop = UISwitch()
    
    override func loadView() {
        super.loadView()
        
        self.view.backgroundColor = UIColor.black
        
        let screenSize = UIScreen.main.bounds.size
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 60))
        headerView.backgroundColor = UIColor.clear
        self.view.addSubview(headerView)
        
        let separatorView = UIView(frame: CGRect(x: 0, y: headerView.frame.maxY - 2, width: screenSize.width, height: 2))
        separatorView.backgroundColor = UIColor.lightGray
        headerView.addSubview(separatorView)
        
        let headerWidths = screenSize.width / 3
        
        let closeBtn = UIButton(type: .system)
        closeBtn.frame = CGRect(x: 0, y: 0, width: headerWidths, height: headerView.bounds.height)
        closeBtn.setTitle("CLOSE", for: .normal)
        closeBtn.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        headerView.addSubview(closeBtn)
        
        let clearBtn = UIButton(type: .system)
        clearBtn.frame = CGRect(x: headerView.frame.maxX - headerWidths, y: 0, width: headerWidths, height: headerView.bounds.height)
        clearBtn.setTitle("CLEAR", for: .normal)
        clearBtn.addTarget(self, action: #selector(clearAction), for: .touchUpInside)
        headerView.addSubview(clearBtn)
        
        let startStopView = UIView(frame: CGRect(x: closeBtn.frame.maxX, y: 0, width: headerWidths, height: headerView.bounds.height))
        startStopView.backgroundColor = UIColor.clear
        headerView.addSubview(startStopView)
        
        let startStopLbl = UILabel(frame: CGRect(x: 0, y: 5, width: headerWidths, height: 15))
        startStopLbl.text = "Start/Stop"
        startStopLbl.textAlignment = .center
        startStopLbl.textColor = UIColor.white
        startStopLbl.font = UIFont.systemFont(ofSize: 15)
        startStopView.addSubview(startStopLbl)
        
        self.startStop.frame = CGRect(x: 0, y: startStopLbl.frame.maxY + 4.5, width: 51, height: 31)
        self.startStop.addTarget(self, action: #selector(startStopAction(_:)), for: .valueChanged)
        self.startStop.center = CGPoint(x: headerWidths / 2, y: self.startStop.center.y)
        startStopView.addSubview(self.startStop)
        
        self.tableView.frame = CGRect(x: 0, y: headerView.frame.maxY, width: screenSize.width, height: screenSize.height - headerView.bounds.height)
        self.tableView.backgroundColor = UIColor.clear
        self.view.addSubview(self.tableView)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.isStatusBarHidden = true
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 75
        self.tableView.separatorStyle = .none
        self.tableView.rowHeight = UITableViewAutomaticDimension
//        self.tableView.register(UINib.init(nibName: "ARConsoleTableViewCell", bundle: nil), forCellReuseIdentifier: "ARConsoleTableViewCell")
        
        startStop.isOn = ARPrint.sharedInstance().isRecording
        
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: NSNotification.Name(ARPrint.NC_ARCONSOLE_CHANGED), object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIApplication.shared.isStatusBarHidden = false
    }
    
    override public var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        reload()
    }
    
    @objc func reload() {
        DispatchQueue.main.async {
            self.consolePrints = ARPrint.sharedInstance().getStrings()
            self.tableView.reloadData()
            if self.consolePrints.count > 0 {
                self.tableView.scrollToRow(at: IndexPath.init(row: self.consolePrints.count - 1, section: 0), at: .bottom, animated: false)
            }
        }
    }
    
    @objc func startStopAction(_ sender: UISwitch) {
        ARPrint.sharedInstance().isRecording = sender.isOn
    }
    
    @objc func closeAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func clearAction() {
        ARPrint.sharedInstance().clearData()
    }
    
    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.consolePrints.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var _cell = tableView.dequeueReusableCell(withIdentifier: "ARConsoleTableViewCell") as? ARConsoleTableViewCell
        
        if _cell == nil {
            _cell = ARConsoleTableViewCell(style: .default, reuseIdentifier: "ARConsoleTableViewCell")
        }
        
        guard let cell = _cell else { return UITableViewCell() }
        
        cell.setView(with: self.consolePrints[indexPath.row])
        
        return cell
    }

}
