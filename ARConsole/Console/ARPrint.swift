//
//  ARPrint.swift
//  CapitalManagement
//
//  Created by Alvaro on 21/8/17.
//  Copyright © 2017 Alvaro Royo. All rights reserved.
//

import UIKit
import AVFoundation

public class ARPrint: NSObject {

    private static var instance:ARPrint? = nil
    public static let defaultColor = UIColor.green
    static let NC_ARCONSOLE_CHANGED = "NC_ARCONSOLE_CHANGED"
    static let ARCONSOLE_PATH = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! + "/ARCONSOLE/"
    static let ARCONSOLE_FILE = "Log.json"
    
    private var printStrings:[ARPrintString] = []
    public var isRecording = false
    
    private var lastDateVolume = Date()
    private var clicks = 0
    public var showWithVolume = true
    
    private var autoSaveQueue = DispatchQueue(label: "Auto Save ARConsole", attributes: .concurrent)
    
//    private let resourceManager = AKResourceManager()
    
    public var maxRecording = 0 //Infinite
    
    private var started = false
    public static func sharedInstance(_ fromPrint:Bool = false) -> ARPrint {
        
        if ARPrint.instance == nil {
            ARPrint.instance = ARPrint()
            
            if fromPrint {
                ARPrint.instance?.start()
            }
            
        }
        
        return ARPrint.instance!
        
    }
    
    /**
     See Docs at https://gitlab.com/ArkitectureRepository/ARConsole
     */
    public func start() {
        
        self.started = true
        self.isRecording = true
        
        loadFromJSON()
        
        self.listenVolumeButton()
        self.autoSave()
    }
    
    /**
     See Docs at https://gitlab.com/ArkitectureRepository/ARConsole
     */
    public func addString(_ str:String, _ tag:String = "---", functionName:String, colour:UIColor = ARPrint.defaultColor) {
        
        if !self.started {
            print("Please start first the console. Read documentation https://gitlab.com/ArkitectureRepository/ARConsole.")
            return
        }
        
        if self.isRecording {
            
            if maxRecording > 0 && printStrings.count == maxRecording {
                printStrings.remove(at: 0)
            }
            
            let str = ARPrintString.init(str, tag: tag, functionName: functionName, colour: colour)
            printStrings.append(str)
            
            consoleChange()
        }
    }
    
    private func autoSave() {
        self.autoSaveQueue.async {
            
            sleep(15)
            
            self.saveJSON()
            
            self.autoSave()
            
        }
    }
    
    private func consoleChange() {
        NotificationCenter.default.post(name: Notification.Name(ARPrint.NC_ARCONSOLE_CHANGED), object: nil)
    }
    
    /**
     See Docs at https://gitlab.com/ArkitectureRepository/ARConsole
     */
    public func showConsole() {
        DispatchQueue.main.async {
            let vc = ARConsoleViewController()
            vc.consolePrints = self.getStrings()
            
            if let window = self.getAppWindow() {
                window.rootViewController?.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    private func getAppWindow() -> UIWindow? {
        
        if let appDelegate = UIApplication.shared.delegate as? NSObject {
            if let wind = appDelegate.value(forKey: "window") as? UIWindow {
                return wind
            }
        }
        
        return nil
        
    }
    
    /**
     See Docs at https://gitlab.com/ArkitectureRepository/ARConsole
     */
    public func getStrings() -> [ARPrintString] {
        let arr = printStrings.sorted{ $0.date < $1.date }
        return arr
    }
    
    /**
     See Docs at https://gitlab.com/ArkitectureRepository/ARConsole
     */
    public func clearData() {
        AKDiskManager.removeFile(ARPrint.ARCONSOLE_PATH + ARPrint.ARCONSOLE_FILE)
        
        printStrings = []
        self.consoleChange()
    }
    
    //MARK: - Load from JSON
    
    private func loadFromJSON() {
        
        if let arr = getJSON() {
            
            for dic in arr {
                
                if let arprintstr = ARPrintString.initialized(with: dic) {
                    self.printStrings.append(arprintstr)
                }
                
            }
            
            var arrObj = getStrings()
            
            if self.maxRecording > 0 {
                
                let diference = arrObj.count - maxRecording
                
                if diference > 0 {
                    
                    for _ in 0..<diference {
                        
                        arrObj.remove(at: 0)
                        
                    }
                    
                    self.printStrings = arrObj
                    
                }
                
            }
            
        }
        
    }
    
    //MARK: - Save JSON
    
    private func saveJSON() {
        
        if !isRecording { return }
        
        var arr:[[String:Any]] = []
        
        for obj in self.printStrings {
            arr.append(obj.toDictionary())
        }
        
        let data = try! JSONSerialization.data(withJSONObject: arr, options: [])
        
        AKDiskManager.writeFile(data: data, path: ARPrint.ARCONSOLE_PATH, fileName: ARPrint.ARCONSOLE_FILE)
        
    }
    
    private func getJSON() -> [[String:Any]]? {
        
        let dat = AKDiskManager.readFile(ARPrint.ARCONSOLE_PATH + ARPrint.ARCONSOLE_FILE)
        
        guard let data = dat else { return nil }
        
        return try! JSONSerialization.jsonObject(with: data, options: []) as! [[String : Any]]
    }
    
    //MARK: - Share file
    
    /**
        See Docs at https://gitlab.com/ArkitectureRepository/ARConsole
    */
    public func shareLogFile(_ completion:((Bool) -> ())? = nil) {
        
        let recordingState = self.isRecording
        self.isRecording = false
        
        self.saveJSON()
        
        let filePath = ARPrint.ARCONSOLE_PATH + ARPrint.ARCONSOLE_FILE
        if FileManager.default.fileExists(atPath: filePath) {
            
            let fileUrl = URL(fileURLWithPath: ARPrint.ARCONSOLE_PATH + ARPrint.ARCONSOLE_FILE)
            let activityVC = UIActivityViewController(activityItems: [fileUrl], applicationActivities: nil)
            
            if let window = getAppWindow() {
                DispatchQueue.main.async {
                    window.rootViewController?.present(activityVC, animated: true, completion: {
                        if let completion = completion { completion(true) }
                    })
                }
            }
            
        } else {
            if let completion = completion { completion(false) }
        }
        
        self.isRecording = recordingState
    }
    
    //MARK: - VOLUME HANDLER ONLY DEBUG MODE
    
    private func listenVolumeButton(){
        
        let audioSession = AVAudioSession.sharedInstance()
        try! audioSession.setActive(true)
        audioSession.addObserver(self, forKeyPath: "outputVolume",
                                 options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "outputVolume" && showWithVolume{
            
            let diference = Date().timeIntervalSince1970 - lastDateVolume.timeIntervalSince1970
            
            if diference < 0.3 {
                clicks += 1
                if clicks == 3 {
                    clicks = 0
                    showConsole()
                }
            } else {
                clicks = 1
            }
            
            lastDateVolume = Date()
            
        }
    }
    
}
